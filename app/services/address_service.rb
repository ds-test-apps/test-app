class AddressService
  POOL_SIZE = 10
  ADDRESS_LOCATION_SIZE = 34
  ADDRESS_LOCATION_REG = [('a'..'h'), ('j'..'z'), ('A'..'H'), ('J'..'N'), ('P'..'Z'), (1..9)].map(&:to_a).flatten

  def generate(count)
    (count || POOL_SIZE).times do
      Address.create(location: generate_location)
    end
  end

  def addresses_pool
    Address.left_joins(:user).where('users.id' => nil).order(created_at: :desc).limit(POOL_SIZE)
  end

  def address_from_pool
    addresses_pool.first
  end

  private

  def generate_location
    (0...ADDRESS_LOCATION_SIZE).map { ADDRESS_LOCATION_REG[rand(ADDRESS_LOCATION_REG.length)] }.join
  end
end
