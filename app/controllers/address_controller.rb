class AddressController < ActionController::API
  before_action :authenticate_user!
  respond_to :json

  def initialize
    @address_service = AddressService.new
    super
  end

  def generate_new
    @user = User.find(params[:user_id])

    if @user == current_user
      address = @address_service.address_from_pool
      if address
        @user.update(address: address)
        render json: address
      else
        bad_request 'Address Pool empty, please try again in 12 Minutes'
      end
    else
      access_denied
    end
  end

  def bad_request(message)
    render json: { error: message }, status: :bad_request
  end

  def access_denied
    render json: { error: 'Access denied.' }, status: :forbidden
  end
end
