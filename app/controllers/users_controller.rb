class UsersController < ApplicationController
  before_action :authenticate_user!

  def initialize
    @address_service = AddressService.new
    super
  end

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    redirect_to root_path, alert: 'Access denied.' if @user != current_user
  end

  def my_address
    @user = User.find(params[:user_id])

    if params[:assign_new]
      @user.update(address: @address_service.address_from_pool) if @user == current_user
    elsif @user == current_user && !@user.address
      @user.update(address: @address_service.address_from_pool)
    end
    redirect_to root_path, alert: 'Access denied.' if @user != current_user
  end
end
