## Check styles
`rubocop`

## Test
`bundle exec rspec`

## API

#### Get JWT token

run command in console, for example:

`curl -i -X POST -d "user[email]"="vnavozenko@gmail.com" -d "user[password]"="123asd123" http://localhost:3000/users/sign_in`

You'll receive response like this:

```
HTTP/1.1 302 Found
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 1; mode=block
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Location: http://localhost:3000/users/1/my_address
Content-Type: text/html; charset=utf-8
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNTQwNzI4ODI2LCJleHAiOjE1NDA3MzI0MjYsImp0aSI6IjQwMTUzMmJlLWNiYmMtNGViMC1iMmQ4LWI1ZmViOTU1OWJlMSJ9.IctVnHb-Mjn5Y4Xjgkj53yEatlZvrOGpgXKd1mQhJNU
Cache-Control: no-cache
Set-Cookie: _test_app_session=xOTMVEURSOx7theuA9ut6%2FwlfjerOK%2Fryv%2FaWg%2BIoE0%2BK31d1fWcx9YLMPYIX7oxxZII6ptjL9S2GCo%2BCL%2F2Vye6cwdcE40KiaGEeFmCnBAhKfsnEG6l8SXfvdtMdrus1jV%2B%2ByOL5g4mBHxP3nJLgN6PwV2Ba98WbtrYNjqJQgRvL%2B%2Big8cvVtc5x%2BKu9tDb04YZEe%2BOcGZUP5mSzx5C%2BBWB9%2F0wrBHCQyPoENg2HFPQTqTK--gId6RO9pKpmKpfZQ--tJt%2Fm0r200tHsZgADUje6Q%3D%3D; path=/; HttpOnly
X-Request-Id: e0031053-dd28-43b1-a058-fdf8109a77c5
X-Runtime: 0.183007
Transfer-Encoding: chunked

```

### Generate new addresses

Endpoint:

`POST localhost:3000/address/:user_id/generate`

Put JWT token in header `Authorization`:

`Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwic2NwIjoidXNlciIsImF1ZCI6bnVsbCwiaWF0IjoxNTQwNzI4ODI2LCJleHAiOjE1NDA3MzI0MjYsImp0aSI6IjQwMTUzMmJlLWNiYmMtNGViMC1iMmQ4LWI1ZmViOTU1OWJlMSJ9.IctVnHb-Mjn5Y4Xjgkj53yEatlZvrOGpgXKd1mQhJNU`

