require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.every '12m', overlap: false, first: :now do
  AddressService.new.generate(10)
end
