Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users

  resources :users do
    get 'my_address', :to => 'users#my_address', as: 'address'
    post 'my_address', :to => 'users#my_address', as: 'assign_new_address'
  end

  post 'address/:user_id/generate', :to => 'address#generate_new'
end
