describe Address do
  before(:each) { @address = Address.new(location: 'NawfaCz4HLsgHRWJXsBvHCMVN3q4l91NuS') }

  subject { @address }

  it { should respond_to(:location) }

  it '#location returns a correct value' do
    expect(@address.location).to match 'NawfaCz4HLsgHRWJXsBvHCMVN3q4l91NuS'
  end
end
